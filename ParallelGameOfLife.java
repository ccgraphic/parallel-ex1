package ex1;

public class ParallelGameOfLife implements GameOfLife {
	// TODO: remove all debug
	public static MiniGameOfLife[][] miniGamesTableDebug;
	public static int rowsDebug;
	public static int colsDebug;
	public static int hSplitDebug;
	public static int vSplitDebug;
	public static final boolean isDebug = false;
	/*
	 * description:
	 * 		the algorithm is:
	 * 		1. calc the threads miniGames size
	 * 		2. initialize all miniBoards
	 * 		3. let each miniGame know of his neighbors
	 * 		4. create threads with their miniGames and the initialField 
	 * 		   and the endFields.
	 * 		5. wait for threads to finish
	 */
	
	public static void main(String[] args) {
		
		//int initialBoardSize=3;
		int hsplit=1;
		int vsplit=2;
		int generations=100;
		
		System.out.println("Starting basic test:"); 
		boolean initialBoard[][] = {
				{ false, true,  false , false, false, false},
				{ false,  false,  true  , false, false, false},
				{ true, true, true ,false, false, false},
				{ false, false, false ,false, false, false},
				{ false, false, false ,false, false, false},
				{ false, false, false ,false, false, false}
				};
		/*initialBoard=new boolean[initialBoardSize][initialBoardSize];
		for (int i=0; i<initialBoardSize; i++) {
			for (int j=0; j<initialBoardSize; j++) {
				initialBoard[i][j]=false;
			}
		}*/
		
		GameOfLife pGol=new ParallelGameOfLife();
		
		pGol.invoke(initialBoard,hsplit,vsplit,generations);
		printAllBoards();
		
	}
	
	public boolean[][][] invoke(boolean[][] initalField, int hSplit, int vSplit,
			int generations) 
	{
		boolean[][][] endFields;
		MiniGameOfLifeThread[][] threadsTable;
		MiniGameOfLife[][] miniGamesTable;
		int width, height;
		
		// TODO: remove!!
		long time = System.nanoTime();
		rowsDebug = initalField.length;
		colsDebug = initalField[0].length;
		hSplitDebug = hSplit;
		vSplitDebug = vSplit;
		
		/* allocate result table*/
		endFields = new boolean[2][][];
		for (int k = 0; k < 2; k++){
			endFields[k] = new boolean[initalField.length][];
			for (int i = 0; i < initalField.length; i++){
				endFields[k][i] = new boolean[initalField[0].length];
			}
		}
		
		/* calc the threads partitions */
		height = initalField.length / vSplit;
		width = initalField[0].length / hSplit;
		
		/* initialize all mini games and threads*/
		miniGamesTable = new MiniGameOfLife[vSplit][];
		miniGamesTableDebug = miniGamesTable; // TODO: remove!!!!
		threadsTable = new MiniGameOfLifeThread[vSplit][];
		for (int i = 0; i < vSplit; i++){
			miniGamesTable[i] = new MiniGameOfLife[hSplit];
			threadsTable[i] = new MiniGameOfLifeThread[hSplit];
			
			for (int j = 0; j < hSplit; j++){
				Indexes currThreadIndexes = new Indexes();
				
				currThreadIndexes.minY = i*height;
				currThreadIndexes.maxY = (i+1)*height - 1;
				currThreadIndexes.minX = j*width;
				currThreadIndexes.maxX = (j+1)*width - 1;
				if (i == vSplit - 1){
					currThreadIndexes.maxY = initalField.length - 1;
				}
				if (j == hSplit - 1){
					currThreadIndexes.maxX = initalField[i].length - 1;
				}
				miniGamesTable[i][j] = new MiniGameOfLife(currThreadIndexes);
				threadsTable[i][j] = 
						new MiniGameOfLifeThread(miniGamesTable[i][j], 
												 generations, 
												 initalField, 
												 endFields);
			}
		}
		
		/* let every game know of his neighbors */
		/* go over all miniGames */
		for (int i = 0; i < vSplit; i++){
			for (int j = 0; j < hSplit; j++){
				MiniGameOfLife[] neighbors = miniGamesTable[i][j].neighbors;
				/* go over neighbors (in clockwise order!)*/
				if (i-1 >= 0){
					if (j-1 >= 0){
						neighbors[0] = miniGamesTable[i-1][j-1];
					}
					neighbors[1] = miniGamesTable[i-1][j];
					if (j+1 < hSplit){
						neighbors[2] = miniGamesTable[i-1][j+1];
					}
				}
				if (j+1 < hSplit){
					neighbors[3] = miniGamesTable[i][j+1];
				}
				if (i+1 < vSplit){
					if (j+1 < hSplit){
						neighbors[4] = miniGamesTable[i+1][j+1];
					}
					neighbors[5] = miniGamesTable[i+1][j];
					if (j-1 >= 0){
						neighbors[6] = miniGamesTable[i+1][j-1];
					}
				}
				if (j-1 >= 0){
					neighbors[7] = miniGamesTable[i][j-1];
				}
			}
		}
		
		/* run the threads */
		for (MiniGameOfLifeThread[] threadsArr : threadsTable){
			for (MiniGameOfLifeThread thread : threadsArr){
				thread.start();
			}
		}
		System.out.println("Parallel initializing is " + (System.nanoTime() - time)/Math.pow(10, 6) + " mili sec");
		
		/* wait for all threads to finish */ 
		for (MiniGameOfLifeThread[] threadsArr : threadsTable){
			for (MiniGameOfLifeThread thread : threadsArr){
				thread.join();
			}
		}
		
		// TODO: remove this
		printResult(endFields);
		
		return endFields;
	}

	/* for debug */
	public static void printAllBoards()
	{
		System.out.println("----All Boards----");
		for (int row = 0; row < rowsDebug; row++){
			for (int i = 0; i < vSplitDebug; i++){
				for (int j = 0; j < hSplitDebug; j++){
					miniGamesTableDebug[i][j].printLine(row);
				}
			}
			System.out.println("");
		}
		System.out.println("--------------------");
	}
	public void printResult(boolean[][][] endFields)
	{
		if (isDebug){
			System.out.println("Result:");
			System.out.println("maxGen - 1:");
			for (int i = 0; i < endFields[0].length; i++){
				for (int j = 0; j < endFields[0][0].length; j++){
					String s = endFields[0][i][j] ? "V " : "X ";
					System.out.print(s);
				}
				System.out.println("");
			}
			System.out.println("");
			System.out.println("maxGen");
			for (int i = 0; i < endFields[1].length; i++){
				for (int j = 0; j < endFields[1][0].length; j++){
					String s = endFields[1][i][j] ? "V " : "X ";
					System.out.print(s);
				}
				System.out.println("");
			}
		}
	}
}

class Cell{
	public boolean bAlive = false;
	public int generation = 0;
	
	public Cell()
	{
		bAlive = false;
		generation = 0;
	}
	public void copy(Cell cell)
	{
		bAlive = cell.bAlive;
		generation = cell.generation;
	}
}

class Indexes{
	public int minY;
	public int maxY;
	public int minX;
	public int maxX;
	
	public Indexes() {}
	public Indexes(int minY, int maxY, int minX, int maxX)
	{
		this.minY = minY;
		this.maxY = maxY;
		this.minX = minX;
		this.maxX = maxX;
	}
	public Indexes(Indexes indexes){
		this.minY = indexes.minY;
		this.maxY = indexes.maxY;
		this.minX = indexes.minX;
		this.maxX = indexes.maxX;
	}
	public void set(Indexes indexes){
		this.minY = indexes.minY;
		this.minX = indexes.minX;
		this.maxY = indexes.maxY;
		this.maxX = indexes.maxX;
	}
}

class ProducerConsumerGOL{
	public Cell[] product;
	
	public ProducerConsumerGOL()
	{
		product = null;
	}
	
	// Call this method after reading neighbor data
	public synchronized Cell[] consume() {
		Cell[] retCells;
		
		/* wait for product to be created */
		while (product == null) {
			try {
				wait(); //TODO : make sure how wait timeout works
	        } catch (Exception e) {}
		}
		
		retCells = product;
		product = null;
		notifyAll();
		return retCells;
	}

	// Call this method after writing data in shared cells (border)
	public synchronized void produce(Cell[] cells) {
		/* wait for product to be consumed */
		while (product != null) {
			try {
	            wait(); //TODO : make sure how wait timeout works
	        } catch (Exception e) {}
		}
		product = cells;
		notifyAll();
	}
	
	public synchronized int tryProduce(Cell[] cells) {
		if (product != null){
			return -1;
		}
		else{
			product = cells;
			notifyAll();
			return 0;
		}
	}
}

/* neighbors are sorted clockwise, 0 - top left corner 
 * when the proper cells are ready they can be produced to neighbors.
 * these cells will first be in the new board. but later, the boards may switch.
 * we can ask by the generation of the cells which one of them we want.*/ 

class MiniGameOfLife{
	public MiniGameOfLife[] neighbors; /* 8 neighbors */
	public ProducerConsumerGOL[] productsForNeighbors;
	public Cell[][] productsReadyForNeighbors;
	public boolean[] isProduced;
	public Cell[][] productsForMe;
	public Cell[][] oldCells;
	public Cell[][] newCells;
	public Indexes indexes;
	public static Object debugSynchPrint; // TODO: remove
	private Cell[] deadCellsArray;
	
	public MiniGameOfLife(Indexes myIndexes)
	{
		// TODO: remove this
		if (debugSynchPrint == null){
			debugSynchPrint = new Object();
		}
		
		indexes = new Indexes();
		indexes.set(myIndexes);
		
		int width = getFullWidth();
		int height = getFullHeight();
		
		/* allocate data structures */
		oldCells = allocateBoard(width, height);
		newCells = allocateBoard(width, height);
		
		productsForNeighbors = new ProducerConsumerGOL[8];
		for (int i = 0; i < 8; i++){
			productsForNeighbors[i] = new ProducerConsumerGOL();
		}
		
		neighbors = new MiniGameOfLife[8];
		productsForMe = new Cell[8][];
		productsReadyForNeighbors = new Cell[8][];
		isProduced = new boolean[8];
		deadCellsArray = new Cell[Math.max(width - 2, height - 2)];
		for (int i = 0; i < deadCellsArray.length; i++){
			deadCellsArray[i] = new Cell();
			deadCellsArray[i].generation = -1; /* because the first thing we do is swap */
		}
	}

	/* description: 
	 * 		copies myIndexes indexes from the bigBoard.
	 * 		at first it copies to the newCells table. 
	 */
	public void initializeBoard(boolean[][] bigBoard)
	{
		int gi, li, gj, lj; /* global and local */
		
		for (gi = indexes.minY, li = 1; gi <= indexes.maxY; gi++, li++){
			for (gj = indexes.minX, lj = 1; gj <= indexes.maxX; gj++, lj++){
				newCells[li][lj].bAlive = bigBoard[gi][gj];
				newCells[li][lj].generation = 0;
				continue;
			}
		}
		/*
		 * try producing all (later will be split)
		 */
		for (int i = 0; i < 8; i++){
			Cell[] cells;
			int ret;
			if (neighbors[i] == null){
				isProduced[i] = true; // always produced...
				continue;
			}
			
			cells = prepareDataForNeighbor(i);
			productsReadyForNeighbors[i] = cells;
			ret = productsForNeighbors[i].tryProduce(productsReadyForNeighbors[i]);
			isProduced[i] = (ret == 0 ? true : false) ;
		}
	}
	
	
	/*
	 * 		0		1		2
	 *			********** 
	 * 		7	**********	3
	 * 			**********
	 *		6		5		4
	 */
	
	/*
	 * description:
	 * 		calculates the indexes need from neighbor 
	 * 		allocates a cells array to put the info in it.
	 * 		neighbor inedexes in global
	 */
	public Cell[] dataForConsumeFromNeighbor(int neighborNum, Indexes neighborIndex) 
	{
		Cell[] neighborCells = null;	
		int width = getFullWidth();
		int height = getFullHeight();
		
		switch (neighborNum){
		case 0:
			neighborCells = new Cell[1];
			neighborCells[0] = new Cell();
			neighborIndex.minX = indexes.minX-1;
			neighborIndex.minY = indexes.minY-1;
			neighborIndex.maxX = indexes.minX - 1;
			neighborIndex.maxY = indexes.minY - 1;
			break;
			
		case 1:
			neighborCells=new Cell[width-2];
			for (int i=0; i<width-2; i++) {
				neighborCells[i]=new Cell();
			}
			neighborIndex.minX=indexes.minX;
			neighborIndex.minY=indexes.minY-1;
			neighborIndex.maxX=indexes.maxX;
			neighborIndex.maxY=indexes.minY-1;
			break;
			
		case 2:
			neighborCells=new Cell[1];
			neighborCells[0]=new Cell();
			neighborIndex.minX=indexes.maxX+1;
			neighborIndex.minY=indexes.minY-1;
			neighborIndex.maxX=indexes.maxX+1;
			neighborIndex.maxY=indexes.minY-1;
			break;
		
		case 3:
			neighborCells=new Cell[height-2];
			for (int i=0; i<height-2; i++) {
				neighborCells[i]=new Cell();
			}
			neighborIndex.minX=indexes.maxX+1;
			neighborIndex.minY=indexes.minY;
			neighborIndex.maxX=indexes.maxX+1;
			neighborIndex.maxY=indexes.maxY;
			break;
			
		case 4:
			neighborCells=new Cell[1];
			neighborCells[0]=new Cell();
			neighborIndex.minX=indexes.maxX+1;
			neighborIndex.minY=indexes.maxY+1;
			neighborIndex.maxX=indexes.maxX+1;
			neighborIndex.maxY=indexes.maxY+1;
			break;
			
		case 5:
			neighborCells=new Cell[width-2];
			for (int i=0; i<width-2; i++) {
				neighborCells[i]=new Cell();
			}
			neighborIndex.minX=indexes.minX;
			neighborIndex.minY=indexes.maxY+1;
			neighborIndex.maxX=indexes.maxX;
			neighborIndex.maxY=indexes.maxY+1;
			break;
			
		case 6:
			neighborCells=new Cell[1];
			neighborCells[0]=new Cell();
			neighborIndex.minX=indexes.minX-1;
			neighborIndex.minY=indexes.maxY+1;
			neighborIndex.maxX=indexes.minX-1;
			neighborIndex.maxY=indexes.maxY+1;
			break;
			

		case 7:
			neighborCells=new Cell[height-2];
			for (int i=0; i<height-2; i++) {
				neighborCells[i]=new Cell();
			}
			neighborIndex.minX=indexes.minX-1;
			neighborIndex.minY=indexes.minY;
			neighborIndex.maxX=indexes.minX-1;
			neighborIndex.maxY=indexes.maxY;
			break;
			
		default:
			ExitWithError.e("bad neighbor");	
		}
		return neighborCells;
	}
	
	/* description: 
	 * 		fills the oldCells board with the cells from the neighbor.
	 * 		from productsForMe array
	 * 
	 * parameters:
	 * 	 	neighborNum - 
	 * 
	 * return: 
	 */
	public int fillCellsFromNeighbor(int neighborNum)
	{         
		int width = getFullWidth();
		int height = getFullHeight();
		Cell[] cells;
		
		if (neighbors[neighborNum] != null){
			cells = productsForMe[neighborNum];
			productsForMe[neighborNum] = null;
		}else{
			cells = deadCellsArray;
			/* can i exit assuming all deadCells have the same reference of the
			 * cells in the array? */
		}
		
		switch(neighborNum) {
		
		case 0:
			oldCells[0][0] = cells[0];
			break;
			
		case 1:
			for (int i = 1; i < width - 1; i++) {
				oldCells[0][i] = cells[i - 1];
			}
			break;
			
		case 2:
			oldCells[0][width - 1] = cells[0];
			break;
		
		case 3:
			for (int i = 1; i < height - 1; i++) {
				oldCells[i][width - 1]=cells[i - 1];
			}
			break;
			
		case 4:
			oldCells[height - 1][width - 1]=cells[0];
			break;
			
		case 5:
			for (int i = 1; i < width - 1; i++) {
				oldCells[height - 1][i]=cells[i - 1];
			}
			break;
			
		case 6:
			oldCells[height - 1][0] = cells[0];
			break;

		case 7:
			for (int i = 1; i < height - 1; i++) {
				oldCells[i][0]=cells[i - 1];
			}
			break;
		}
		
		return 0;
			
	}
	
	/* description:
	 * 		fills array with the cells from the oldCells(!) board
	 * 
	 * parameters:
	 * 		cells: one dimension array
	 * 		indexes: the indexes asked by the global (!) indexes
	 */
	public void getLineFromNew(Cell[] cells, Indexes indexes)
	{
		int cellsIterator, width, height;
		
		cellsIterator = 0;
		/* width & height are the total num of cells to run on */
		width = indexes.maxX - indexes.minX + 1;
		height = indexes.maxY - indexes.minY + 1;
		
		/* validation */
		if (	(width != 1 && height != 1)		 ||
				indexes.minY < this.indexes.minY || 
				indexes.maxY > this.indexes.maxY ||
				indexes.minX < this.indexes.minX || 
				indexes.maxX > this.indexes.maxX )
		{
			ExitWithError.e("bad neighbor request");
		}
		
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){
				int localI = globalToLocalI(indexes.minY + i);
				int localJ = globalToLocalJ(indexes.minX + j);
				
				cells[cellsIterator].copy(newCells[localI][localJ]);
				cellsIterator++;
			}
		}
	}
	
	public void tryProduceAll()
	{
		// TODO: fill this!!! double code in initialize and fill
	}
	/* description: 
	 * 		the regular algorithm for filling the board.
	 * 		1. consume info from neighbors
	 * 		2. fill board
	 * 		3. switch the tables.
	 * 		maybe we will make more sophisticated later.		
	 */
	public int fillBoard()
	{ 
	    /*only when I have all the info needed - depends on the per thread optimization
	    need to call produceToNeighbor on appropriate times 
		*/
		
		int width = getFullWidth();
		int height = getFullHeight();
		
		/* fill the outside first and try producing it */
		/* first and last columns */
		for (int i = 1; i < height - 1; i++){
			calculateCell(i, 1);
			if (width <= 3){
				/* there is only one inside columns */
				continue;
			}
			calculateCell(i, width - 2);
		}
		/* first and last rows without corners */
		for (int j = 2; j < width - 2; j++){
			calculateCell(1, j);
			if (height <= 3){
				/* there is only one inside row */
				continue;
			}
			calculateCell(height - 2, j);
		}
		/*
		 * try producing all (later will be split)
		 */
		for (int i = 0; i < 8; i++){
			Cell[] cells;
			int ret;
			if (neighbors[i] == null){
				isProduced[i] = true; // always produced...
				continue;
			}
			
			cells = prepareDataForNeighbor(i);
			productsReadyForNeighbors[i] = cells;
			ret = productsForNeighbors[i].tryProduce(productsReadyForNeighbors[i]);
			isProduced[i] = (ret == 0 ? true : false) ;
		}
		
		/* calculate inside */
		for (int i=2; i<height-2; i++) {
			for (int j=2; j<width-2; j++) {
				calculateCell(i,j);
			}
		}

		return 0;
	}
	
	/* description: 
	 * 		find the next generation status of this cell depending his neighbors
	 * 		1. A dead cell with exactly three live neighbors becomes a live cell (birth).	
	 * 		2. A live cell with two or three live neighbors stays alive (survival).
	 *		3. In all other cases, a cell dies or remains dead (overcrowding or loneliness).
	 */
	public int calculateCell(int row, int col)
	{ 
		int width = getFullWidth();
		int height = getFullHeight();
		
		if (row < 1 || col < 1 ||  col >= width-1 || row >= height-1) {
			ExitWithError.e("calculateCell: x,y out of limitations");
		}
		
	    int liveNeighbors=0;
	    for (int i = row - 1; i < row + 2; i++) {
	    	for (int j = col - 1; j < col + 2; j++) {
	    		if (i!=row || j!=col) {
	    			if (oldCells[i][j].generation!=oldCells[row][col].generation) {
	    				ExitWithError.e("calculateCell: neighbors not with same generation of calculated cell");
	    			}
	    			if (oldCells[i][j].bAlive==true) {
	    				liveNeighbors++;
	    			}
	    		}
	    	}
	    }
	    
	    newCells[row][col].bAlive=false;
	    newCells[row][col].generation=oldCells[row][col].generation+1;
	    if (oldCells[row][col].bAlive==false && liveNeighbors==3) {
	    	newCells[row][col].bAlive=true;
	    } else if (oldCells[row][col].bAlive==true && (liveNeighbors==2 || liveNeighbors==3)) {
	    	newCells[row][col].bAlive=true;
	    }
	    
		return 0;
	}
	
	/*
	 * description:
	 * 		allocates and prepares the data needed by neighbor
	 * 		'neighborNum'.
	 * 
	 */
	private Cell[] prepareDataForNeighbor(int neighborNum)
	{
		Cell[] neighborCells = null;
		Indexes neighborIndex = new Indexes();
		int width = getFullWidth();
		int height = getFullHeight();
		
		if (neighbors[neighborNum] == null){
			ExitWithError.e("bad neighbor");
		}
		
		switch (neighborNum){
		case 0:
			neighborCells = new Cell[1];
			neighborCells[0] = new Cell();
			neighborIndex.minX = indexes.minX;
			neighborIndex.minY = indexes.minY;
			neighborIndex.maxX = indexes.minX;
			neighborIndex.maxY = indexes.minY;
			break;
			
		case 1:
			neighborCells = new Cell[width - 2];
			for (int i = 0; i < width - 2; i++) {
				neighborCells[i] = new Cell();
			}
			neighborIndex.minX = indexes.minX;
			neighborIndex.minY = indexes.minY;
			neighborIndex.maxX = indexes.maxX;
			neighborIndex.maxY = indexes.minY;
			break;
			
		case 2:
			neighborCells = new Cell[1];
			neighborCells[0] = new Cell();
			neighborIndex.minX = indexes.maxX;
			neighborIndex.minY = indexes.minY;
			neighborIndex.maxX = indexes.maxX;
			neighborIndex.maxY = indexes.minY;
			break;
		
		case 3:
			neighborCells = new Cell[height - 2];
			for (int i = 0; i < height - 2; i++) {
				neighborCells[i] = new Cell();
			}
			neighborIndex.minX = indexes.maxX;
			neighborIndex.minY = indexes.minY;
			neighborIndex.maxX = indexes.maxX;
			neighborIndex.maxY = indexes.maxY;
			break;
			
		case 4:
			neighborCells = new Cell[1];
			neighborCells[0] = new Cell();
			neighborIndex.minX = indexes.maxX;
			neighborIndex.minY = indexes.maxY;
			neighborIndex.maxX = indexes.maxX;
			neighborIndex.maxY = indexes.maxY;
			break;
			
		case 5:
			neighborCells = new Cell[width - 2];
			for (int i = 0; i < width - 2; i++) {
				neighborCells[i] = new Cell();
			}
			neighborIndex.minX = indexes.minX;
			neighborIndex.minY = indexes.maxY;
			neighborIndex.maxX = indexes.maxX;
			neighborIndex.maxY = indexes.maxY;
			break;
			
		case 6:
			neighborCells = new Cell[1];
			neighborCells[0] = new Cell();
			neighborIndex.minX = indexes.minX;
			neighborIndex.minY = indexes.maxY;
			neighborIndex.maxX = indexes.minX;
			neighborIndex.maxY = indexes.maxY;
			break;
			

		case 7:
			neighborCells = new Cell[height - 2];
			for (int i = 0; i < height - 2; i++) {
				neighborCells[i] = new Cell();
			}
			neighborIndex.minX = indexes.minX;
			neighborIndex.minY = indexes.minY;
			neighborIndex.maxX = indexes.minX;
			neighborIndex.maxY = indexes.maxY;
			break;
			
		default:
			ExitWithError.e("bad neighbor");	
		}
		
		getLineFromNew(neighborCells, neighborIndex);
		return neighborCells;
	}
	
	/* description:
	 * 		let neighbor know that the info he needs is ready.
	 * 		for now it is triggered after switching boards.
	 * 
	 * 		TODO later: trigger when the info is ready in the newCells table.
	 * 					later on, it can be moved to the oldCells table.
	 */
	public int produceToNeighbor(int neighbor)
	{ 
	    /*by the indexes mark the neighbor's readyToProduce and notify if needed */
		if (neighbors[neighbor] == null){
			return 0;
		}
		productsForNeighbors[neighbor].produce(productsReadyForNeighbors[neighbor]);
		isProduced[neighbor] = true;
		productsReadyForNeighbors[neighbor] = null;
		return 0;
	}
	
	public boolean wasProductProduced(int neighbor)
	{
		return isProduced[neighbor];
	}
	
	/*
	 * description: 
	 * 		allocates a new cells board of dimensions width x height
	 */
 	private Cell[][] allocateBoard(int width, int height)
	{
		Cell[][] cells = new Cell[height][];
		for (int i = 0; i < height; i++){
			cells[i] = new Cell[width];
			for (int j = 0; j < width; j++){
				cells[i][j] = new Cell();
			}
		}
		return cells;
	}
	
	public void printBoard() {
		if (ParallelGameOfLife.isDebug){
			int width = getFullWidth();
			int height = getFullHeight();
			synchronized(debugSynchPrint){
				System.out.println("Thread board: indexes: Min.X:"+indexes.minX+" Min.Y:"+indexes.minY+" Max.X:"+indexes.maxX
						+" Max.Y:"+indexes.maxY + " Generation: "+oldCells[1][1].generation);   
				for (int i=1; i<height-1; i++) {
					for (int j=1; j<width-1; j++) {
						if (oldCells[i][j].bAlive==false) {
							System.out.print("X ");
						} else {
							System.out.print("V ");
						}
					}
					System.out.print("\n");
				}
				System.out.println("");
			}
		}
	}

	/*
	 * description:
	 * 		need to change this name!!!
	 */
	public void switchBoards()
	{
		//switch boards
		Cell[][] tempboard = oldCells;
		oldCells = newCells;
		newCells = tempboard;
		
		/* also advance the deadCellsArray for the calculation */
		for (int i = 0; i < deadCellsArray.length; i++){
			deadCellsArray[i].generation++;
		}
	}

	private int mirrorMeInNeighbor(int neighbor)
	{
		/* just turns out to be this way... */
		return (neighbor + 4)%8;
	}
	
	public int consumeFromNeighbor(int neighbor)
	{
		if (neighbors[neighbor] != null) {
			int meInNeighbor = mirrorMeInNeighbor(neighbor);
			productsForMe[neighbor] = neighbors[neighbor].productsForNeighbors[meInNeighbor].consume();
		}
		
		fillCellsFromNeighbor(neighbor);
		
		return 0;
	}

	public int getFullWidth()
	{
		return indexes.maxX - indexes.minX + 1 + 2;
	}
	public int getFullHeight()
	{
		return indexes.maxY - indexes.minY + 1 + 2;
	}
	public int globalToLocalI(int i)
	{
		return (i - indexes.minY) + 1;
	}
	public int globalToLocalJ(int j)
	{
		return (j - indexes.minX) + 1;
	}
	public int localToGlobalI(int i)
	{
		return (i - 1) + indexes.minY;
	}
	public int localToGlobalJ(int j)
	{
		return (j - 1) + indexes.minX;
	}
	private void fillResultTable(boolean[][] resultTable, Cell[][] cellsTable)
	{
		int height = getFullHeight();
		int width = getFullWidth();
		int li, gi, lj, gj;
		
		for (li = 1, gi = localToGlobalI(1); li < height - 1; li++, gi++){
			for (lj = 1, gj = localToGlobalJ(1); lj < width - 1; lj++, gj++){
				resultTable[gi][gj] = cellsTable[li][lj].bAlive;
			}
		}
	}
	public void fillResultTables(boolean[][][] resultTables)
	{
		fillResultTable(resultTables[0], oldCells);
		fillResultTable(resultTables[1], newCells);
	}

	/* for debug */
	public void printLine(int globalRow)
	{
		int width = getFullWidth() - 2;
		int lRow = globalToLocalI(globalRow);
		if (globalRow < indexes.minY || globalRow > indexes.maxY){
			return;
		}
		for (int j = 0; j < width; j++){
			String s = newCells[lRow][j+1].bAlive ? "V " : "X ";
			System.out.print(s);
		}
		System.out.print(" ");
	}
}

class MiniGameOfLifeThread implements Runnable{
	private Thread thread;
	public MiniGameOfLife miniGame;
	public int totGenerations = 0;
	public boolean initialBoard[][];
	private boolean[][][] resultTables;
	
	public MiniGameOfLifeThread(
			MiniGameOfLife miniGame, 
			int gens, 
			boolean initBoard[][],
			boolean[][][] resultTables)
	{
		this.miniGame = miniGame;
		this.totGenerations = gens;
		initialBoard = initBoard;
		this.resultTables = resultTables;
	}
	
	public void run(){
			
		/* initialize with input board (puts data into newCells)*/
		miniGame.initializeBoard(initialBoard);
		
		/* producer consumer data is initialized so you can start producing */
		
		while (miniGame.newCells[1][1].generation < totGenerations) {
			
			miniGame.switchBoards();
			
			// produce to neighbors. all data is in oldCells
			for (int i = 0; i < 8; i++){
				if (miniGame.wasProductProduced(i)){
					continue;
				}
				/* was not produced */
				miniGame.produceToNeighbor(i);
			}
			
			// consume from neighbors
			for (int i = 0; i < 8; i++) {
				miniGame.consumeFromNeighbor(i);
			}
			
			// TODO: remove! For debug oldCells
			miniGame.printBoard();
			
			// fillboard
			miniGame.fillBoard();
			
		}
		
		/* fill result boards */
		miniGame.fillResultTables(resultTables);
	}
	public void start()
	{
		thread = new Thread(this);
		thread.start();
	}
	public void join()
	{
		try {
			thread.join();
		} catch (InterruptedException e) {
			// e.printStackTrace();
			ExitWithError.e("");
		}
	}
}

class ExitWithError{
	public static void e(String str){
		System.out.println(str);
		Thread.dumpStack();
		System.exit(-1);
	}
}